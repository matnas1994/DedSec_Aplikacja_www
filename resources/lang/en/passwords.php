<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Hasło musi zawierać 6 znaków i być poprawne.',
    'reset' => 'Hasło zostało zresetowane!',
    'sent' => 'Wysłaliśmy twoje hasło na maila!',
    'token' => 'This password reset token is invalid.',
    'user' => "Nie znaleźlismy takie użytkownika.",

];
