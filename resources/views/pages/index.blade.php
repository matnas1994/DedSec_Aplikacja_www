@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Pages</div>
                    <a class="btn btn-primary" href="{{route('pages.create')}}">Dodaj stronę</a>
                    <table class="table table-hover">
                        <tr>
                            <th>ID</th>
                            <th>TITLE</th>
                            <th>OPTIONS</th>
                        </tr>
                        @foreach($pages as $page)
                            <tr>
                                <td>{{ $page->id }}</td>
                                <td>{{ $page->title  }}</td>
                                <td><a class="btn btn-info" href="{{route('pages.edit',$page)}}">Edit</a>
                                <td>    {!! Form::model($page, ['route' => ['pages.delete', $page], 'method' => 'DELETE']) !!}
                                 <button class="btn btn-danger">Delete</button>
                                    {!! Form::close() !!}</td>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                    <center> {{ $pages->links() }} </center>
                </div>
            </div>
        </div>
    </div>
@endsection