@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Pages</div>
                    {!! Form::model($page, ['route' => ['pages.update', $page], 'method' => 'PUT']) !!}

                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger">{{ $error }}</div>
                        @endforeach
                    @endif
                    <div class="form-group">
                        {!! Form::label('title', "Title:") !!}
                        {!! Form::text('title', $page->title, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('content', "Treść:") !!}
                        {!! Form::textarea('content', $page->content, ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Zapisz', ['class'=>'btn btn-primary ']) !!}
                        {!! link_to(URL::previous(), 'Powrót', ['class' => 'btn btn-default']) !!}
                    </div>


                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection